import random

#-------------- Task 1 -----------------#
numbers_arr = []
a = 1
while a == 1:
    temp =  int(input("Будь ласка введіть ціле число. Для того щоб закінчити наповнення введіть 0 : "))
    if temp == 0:
        break
    else:
        numbers_arr.append(temp)

print("Максимальний елемент = " + str(max(numbers_arr)))

numbers_arr.sort()
numbers_arr.reverse()
print("Список на екран у зворотному порядку = " + str(numbers_arr))
#-------------- End Task 1 -----------------#


#-------------- Task 2 -----------------#
positive_numbers = []
negative_numbers = []
a = 1
while a == 1:
    temp =  int(input("Будь ласка введіть ціле число. Для того щоб закінчити наповнення введіть 0 : "))
    if temp > 0:
        positive_numbers.append(temp)
    elif temp < 0:
        negative_numbers.append(temp)
    else:
        break

print("додатні елементи = " + str(positive_numbers))
print("решта = " + str(negative_numbers))
#-------------- End Task 2 -----------------#


#-------------- Task 3 -----------------#
summ = 0
for i in range(1,21):
    if i % 2 == 0:
        continue
    else:
        summ += i


print("Сумма не парних елементів = " + str(summ))
#-------------- End Task 3 -----------------#


#-------------- Task 4 -----------------#
numbers = []
for i in range(30):
    numbers.append(int(random.triangular(-100, 100)))

max_element = max(numbers)
index_max_element = numbers.index(max_element)
print("Максимальне число в списку : " + str(max_element))
print("Індекс максимального числа в списку : " + str(index_max_element))

even_numbers = []
for i in numbers:
    if i % 2 == 0:
        continue
    else:
        even_numbers.append(i)

if even_numbers:
    even_numbers.sort()
    even_numbers.reverse()
    print("Список непарних елементів : " + str(even_numbers))
else:
    print("Непарних елементів не знайденно!")
#-------------- End Task 4 -----------------#


#-------------- Task 5 -----------------#
numbers = []
for i in range(30):
    numbers.append(int(random.triangular(-100, 100)))

negative_pair_numbers = []
index = 0
for element in numbers:
    if index == len(numbers) - 1: break

    next_element = numbers[index + 1]
    if element < 0 and next_element < 0:
        print("Пара відємних елементів " + str(element) + " , " + str(next_element))
    index += 1
#-------------- End Task 5 -----------------#


#-------------- Task 6 -----------------#
numbers = []
for i in range(10):
    numbers.append(int(random.triangular(2, 100)))

index_max_element = numbers.index(max(numbers))
numbers = numbers[:index_max_element] + numbers[index_max_element+1:]

new_numbers = []
for i in numbers:
    new_numbers.append(i**2)
new_numbers.sort()
new_numbers.reverse()
print("Список квадратів чисел : " + str(new_numbers))
#-------------- End Task 6 -----------------#


#-------------- Task 7 -----------------#
numbers = []
for _ in range(30):
    numbers.append(random.triangular(-100, 100))

sortedAbsNumbers = []
for number in range(30):
    sortedAbsNumbers.append(abs(numbers[number]))

numbers.sort()
sortedAbsNumbers.sort()

print("Найменше число по модулю:", sortedAbsNumbers[:1])
print("Відсортований список елементів", numbers)
#-------------- End Task 7 -----------------#


#-------------- Task 8 -----------------#
numbers = []
for _ in range(30):
    numbers.append(random.triangular(-100, 100))

index = 0
for _ in range(10):
    temp = []
    temp.append(abs(numbers[index]))
    temp.append(abs(numbers[index + 1]))
    temp.append(abs(numbers[index + 2]))
    temp.sort()
    print("Списки за зростанням окремих елементів : " + str(temp))
    index += 3
#-------------- End Task 8 -----------------#

#-------------- Task 1 -----------------#
a = int(input("Будь ласка введіть ціле число: "))
b = int(input("Будь ласка введіть ціле число: "))
c = float(input("Будь ласка введіть дробове число: "))
d = float(input("Будь ласка введіть дробове число: "))
#-------------- End Task 1 -----------------#


#-------------- Task 2 -----------------#
def addition(a, b):
    return a + b

def subtraction(a, b):
    return a - b

def multiplication(a, b):
    return a * b

def division(a, b):
    return a / b

def _pow(a, b):
    return pow(a, b)

def div(a, b):
    try:
        res = a // b
        return res
    except ZeroDivisionError:
        print('Неможливо поділити на 0!')

def mod(a, b):
    try:
        res = a % b
        return res
    except ZeroDivisionError:
        print('Неможливо поділити на 0!')

listOfNumber = []
listOfNumber.append(addition(a,b))
listOfNumber.append(subtraction(a,b))
listOfNumber.append(multiplication(a,b))
listOfNumber.append(division(a,b))
listOfNumber.append(_pow(a,b))
listOfNumber.append(div(a,b))
listOfNumber.append(mod(a,b))
print("Результат виконаних операцій : " + str(listOfNumber))
#-------------- End Task 2 -----------------#


#-------------- Task 3 -----------------#
print("Кількість елементів у списку : " + str(len(listOfNumber)))
#-------------- End Task 3 -----------------#


#-------------- Task 4 -----------------#
listOfNumber[2], listOfNumber[5] = listOfNumber[5], listOfNumber[2]
print("Список після перестановки елементів: " + str(listOfNumber))
#-------------- End Task 4 -----------------#


#-------------- Task 5 -----------------#
name = input("Введіть ваше прізвище та ім’я.")
print('Я ' + name + ' виконав лабороторну роботу №1 Висновки:\nпід час виконання лабораторної роботи\nбуло набуто практичні навички щодо алгоритмів послідовної (лінійної) структури,\nз процедурами запуску програм на мові Python.')
#-------------- End Task 5 -----------------#

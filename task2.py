import math

#-------------- Task 1 -----------------#
a = int(input("Будь ласка введіть ціле число: "))
b = int(input("Будь ласка введіть ціле число: "))
c = int(input("Будь ласка введіть ціле число: "))

if a >= 1 and a <= 3:
    print("Число " + str(a) + " належить інтервалу [1,3].")
if b >= 1 and b <= 3:
    print("Число " + str(b) + " належить інтервалу [1,3].")
if c >= 1 and c <= 3:
    print("Число " + str(c) + " належить інтервалу [1,3]." )
#-------------- End Task 1 -----------------#


#-------------- Task 2 -----------------#
year = int(input("Будь ласка введіть ціле число щоб дізнатись скільки днів у цьому році: "))
if year % 4 == 0 and year % 100 != 0:
    print("Це високосний рік 366 днів")
elif year % 400 == 0:
    print("Це високосний рік 366 днів")
else:
    print("Це невисокосний рік 365 днів")
#-------------- End Task 2 -----------------#


#-------------- Task 3 -----------------#
summ = float(input("Будь ласка введіть суму вашої покупки: "))
if summ > 1000:
    disscount = 5
    print("Ваша ціна з урахуванням знижки " + str(disscount) + "% всього " + str(summ - (summ / 100 * disscount)))
elif summ > 500:
    disscount = 3
    print("Ваша ціна з урахуванням знижки " + str(disscount) + "% всього " + str(summ - (summ / 100 * disscount)))
else:
    print("Ваша ціна  всього " + str(summ))
#-------------- End Task 3 -----------------#


#-------------- Task 4 -----------------#
a = int(input("Будь ласка введіть ціле число: "))
b = int(input("Будь ласка введіть ціле число: "))
c = int(input("Будь ласка введіть ціле число: "))
d = int(input("Будь ласка введіть ціле число: "))

min_number = a
if b < min_number:
    min_number = b
if c < min_number:
    min_number = c
if d < min_number:
    min_number = d

print('Найменше введене число: ', min_number)
print('Косинус найменшого числа: ', math.cos(min_number))
#-------------- End Task 4 -----------------#


#-------------- Task 5 -----------------#
a = int(input("Будь ласка введіть ціле число: "))
b = int(input("Будь ласка введіть ціле число: "))
c = int(input("Будь ласка введіть ціле число: "))

max_number = a
if b > max_number:
    max_number = b
if c > max_number:
    max_number = c

print('Найбільше введене число: ', max_number)
print('Синус найбільшого числа: ', math.sin(max_number))
#-------------- End Task 5 -----------------#


#-------------- Task 6 -----------------#
a = int(input("Будь ласка введіть ціле число сторони рівнобедреного трикутника: "))
b = int(input("Будь ласка введіть ціле число опори рівнобедреного трикутник: "))
p = (a + a + b) / 2
s = math.sqrt(p*(p-a)*(p-a)*(p-b))

if s % 2 == 0:
    print("Площа трикутника = " + str(s / 2))
else:
    print("Не можу ділити на 2!")
#-------------- End Task 6 -----------------#


#-------------- Task 7 -----------------#
numberOfMonth = int(input("Будь ласка введіть номер потрібного вам місяця: "))

if numberOfMonth == 1:
    print("January")
elif numberOfMonth == 2:
    print("February")
elif numberOfMonth == 3:
    print("March")
elif numberOfMonth == 4:
    print("April")
elif numberOfMonth == 5:
    print("May")
elif numberOfMonth == 6:
    print("June")
elif numberOfMonth == 7:
    print("July")
elif numberOfMonth == 8:
    print("August")
elif numberOfMonth == 9:
    print("September")
elif numberOfMonth == 10:
    print("October")
elif numberOfMonth == 11:
    print("November")
elif numberOfMonth == 12:
    print("December")
else:
    print("Місяця за цим номером не існує")
# -------------- End Task 7 -----------------#


#-------------- Task 8 -----------------#
a = int(input("Будь ласка введіть ціле число : "))
b = int(input("Будь ласка введіть ціле число : "))
c = int(input("Будь ласка введіть ціле число : "))

if a > 0 and b > 0 and c > 0:
    print("3 позитивні числа")
elif (a > 0 and b > 0 and c <= 0) or (a > 0 and b <= 0 and c > 0) or (a <= 0 and b > 0 and c > 0):
    print("2 позитивні числа")
elif (a > 0 and b <= 0 and c <= 0) or (a <= 0 and b > 0 and c <= 0) or (a <= 0 and b <= 0 and c > 0):
    print("1 позитивне число")
else:
    print("Не має позитивних чисел")
#-------------- End Task 8 -----------------#


#-------------- Task 9 -----------------#
a = int(input("Будь ласка введіть ціле число : "))
b = int(input("Будь ласка введіть ціле число : "))

summ = 0
for i in range(a, b + 1):
    summ += i

print(summ)
#-------------- End Task 9 -----------------#


#-------------- Task 10 -----------------#
a = int(input("Будь ласка введіть ціле число : "))
b = int(input("Будь ласка введіть ціле число : "))

summ = 0
for i in range(a, b + 1):
    summ += i*i

print(summ)
#-------------- End Task 10 -----------------#


#-------------- Task 11 -----------------#
a = int(input("Будь ласка введіть ціле число <= 200: "))
b = 200

summ = 0
for i in range(a, b):
    summ += i

mid = summ / (b - a)
print("Cереднє арифметичне = " + str(mid))
#-------------- End Task 11 -----------------#


#-------------- Task 12 -----------------#
a = int(input("Будь ласка введіть ціле число: "))
b = int(input("Будь ласка введіть ціле число більше за попереднє: "))

counter = -1
summ = 0

while b > a:
    if counter == b - a:
        break
    else:
        counter += 1
        summ =summ + a + counter

print(summ)
#-------------- End Task 12 -----------------#


#-------------- Task 13 -----------------#
a = int(input("Будь ласка введіть ціле число : "))
b = 50

summ = 0
for i in range(a, b + 1):
    summ += i*i

print(summ)
#-------------- End Task 13 -----------------#


#-------------- Task 14 -----------------#
N = int(input("Будь ласка введіть ціле число: "))
K = 2

while K < N:
    if (5 ** K > N):
        print('Найменше число К = ' + str(K))
        break
    else:
        K += 1
#-------------- End Task 14 -----------------#


#-------------- Task 15 -----------------#
a = int(input("Будь ласка введіть ціле число від 0 до 24: "))
arr = [1, 4, 9, 16, 25]

for i in range(len(arr)):
    if (arr[i] > a):
        print('перше число, більше за ' + str(a) + " це число = " + str(arr[i]))
        break
#-------------- End Task 15 -----------------#


#-------------- Task 16 -----------------#
a = int(input("Будь ласка введіть ціле число від 0 до 22: "))
arr = [1, 5, 10, 16, 23]

counter = 0
while a > 0:
    if arr[counter] > a:
        print('перше число, більше за ' + str(a) + " це число = " + str(arr[counter]))
        break
    else:
        counter += 1
#-------------- End Task 16 -----------------#


import re

#-------------- Task 1 -----------------#
text = "«Реа́л Мадри́д» (ісп. Real Madrid Club de Fútbol) — іспанський футбольний клуб із Мадрида. Засновано 6 березня 1902 року. Один із найвідоміших і найсильніших клубів Іспанії та світу. Найкращий футбольний клуб ХХ століття за версією ФІФА[2]."
letter = input("Введіть літеру: ")
result = len([t for t in text.lower().split() if t.startswith(letter.lower())])
print("Початковий тест:")
print(text)
print("В тесті знайдено {} слів, що починаються на літеру {}".format(result, letter))
#-------------- End Task 1 -----------------#


#-------------- Task 2 -----------------#
text = "Lorem, ipsum : dolor sit amet :consectetur: adipisicing : :: elit. Facilis, repellendus?"
count = text.count(":")
replacedText = text.replace(":", "%")
print("Змінений текст: " + replacedText)
print("Здійснено {} замін".format(count))
#-------------- End Task 2 -----------------#


#-------------- Task 3 -----------------#
text = "Lorem, ipsum. dolor sit amet .consectetur. adipis..icing. elit. Facilis, repellendus?"
dotCount = text.count(".")
replacedText = text.replace(".", "")
print("Початковий текст: " + text)
print("Змінений текст: " + replacedText)
print("Видалено {} символів '.'".format(dotCount))
#-------------- End Task 3 -----------------#


#-------------- Task 4 -----------------#
text = "«Реа́л Мадри́д» (ісп. Real Madrid Club de Fútbol) — іспанський футбольний клуб із Мадрида. Засновано 6 березня 1902 року. Один із найвідоміших і найсильніших клубів Іспанії та світу. Найкращий футбольний клуб ХХ століття за версією ФІФА[2]."
replacedText = text.replace("а", "о")
replacedCount = text.count("а")
print("Змінений текст: "+ replacedText)
print("Здійснено {} замін(и)".format(replacedCount))
print("В рядку {} символів".format(len(text)))
#-------------- End Task 4 -----------------#


#-------------- Task 5 -----------------#
text = "Lorem, ipsum dolor SIT amet. Consectetur adipisicing elit. Facilis, Repellendus?"

print("Початковий текст: " + text)
print("Змінений текст: " + text.lower())
#-------------- End Task 5 -----------------#


#-------------- Task 6 -----------------#
text = "«Реа́л Мадри́д» (ісп. Real Madrid Club de Fútbol) — іспанський футбольний клуб із Мадрида. Засновано 6 березня 1902 року. Один із найвідоміших і найсильніших клубів Іспанії та світу. Найкращий футбольний клуб ХХ століття за версією ФІФА[2]."
count = text.count("о")
replacedText = text.replace("о", "")
print("Змінений текст: " + replacedText)
print("Видалено {} символів 'о'".format(count))
#-------------- End Task 6 -----------------#

#-------------- Task 7 -----------------#
string = "привіт, предмет, практика, письмо, помідор, перехрестя"

halfLengthOfString = int(len(string) / 2)
string = string[:halfLengthOfString].replace('п', '*') + string[halfLengthOfString:]
print(string)
#-------------- End Task 7 -----------------#

#-------------- Task 8 -----------------#
text = "«Реа́л Мадри́д» (ісп. Real Madrid Club de Fútbol) — іспанський футбольний клуб із Мадрида. Засновано 6 березня 1902 року. Один із найвідоміших і найсильніших клубів Іспанії та світу. Найкращий футбольний клуб ХХ століття за версією ФІФА[2]."
word = input("Введіть слово: ")
result = text.count(" {} ".format(word))
print("Початковий тест: " + text)
print("В тесті знайдено {} рази слово '{}'".format(result, word))
#-------------- End Task 8 -----------------#

#-------------- Task 9 -----------------#
text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
capitalizedText = text.title()
print("Форматований тест:\n" + capitalizedText)
#-------------- End Task 9 -----------------#

#-------------- Task 10 -----------------#
text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
startLetter = input("Введіть початкову літеру: ")
endLetter = input("Введіть кінцеву літеру: ")
words = re.findall(rf"\b{startLetter}\S+{endLetter}\b", text)
print(f"Початковий тест:\n {text}")
print(f"Знайдені слова: {words}")
#-------------- End Task 10 -----------------#

#-------------- Task 11 -----------------#
text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
textLowercase = text.lower()
vowels = "aeiou"
counter = 0
for v in vowels:
    counter += textLowercase.count(v)

print("Початковий тест: \n" + text)
print(f"В тексті знайдено {counter} голосних літер")
#-------------- End Task 11 -----------------#

#-------------- Task 12 -----------------#
text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
textLovercase = text.lower()
consonant = "bcdfghjklmnpqrstvwxzy"
counter = 0
for c in consonant:
    counter += textLovercase.count(c)

print("Початковий тест: \n" + text)
print(f"В тексті знайдено {counter} приголосних літер")
#-------------- End Task 12 -----------------#

#-------------- Task 13 -----------------#
text = "Lorem Ipsum is simply dummy text of the printing and Typesetting industry. Lorem Ipsum has been the industry's standard Dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
words = []
for word in text.split(' '):
    if word[0].isupper():
        words.append(word)
print(f"Перелік власних назв:\n {words}")
#-------------- End Task 13 -----------------#

